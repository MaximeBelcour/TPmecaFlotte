
# +----------------------------------------------------------------------+
# | Coordinate                                                           |
# +----------------------------------------------------------------------+

COORDINATE {
    coordinates                         = Read("MESH.DIR/CFDAnalysis.crd.B")
}

# +----------------------------------------------------------------------+
# | Volume Set                                                           |
# +----------------------------------------------------------------------+

VOLUME_SET( "CFDAnalysis.SolidBody_1_1.tet4" ) {
    elements                            = Read("MESH.DIR/CFDAnalysis.SolidBody_1_1.tet4.cnn.B")
    shape                               = four_node_tet
}

# +----------------------------------------------------------------------+
# | Volume Set                                                           |
# +----------------------------------------------------------------------+

VOLUME_SET( "CFDAnalysis.SolidBody_1_1.pyr5" ) {
    elements                            = Read("MESH.DIR/CFDAnalysis.SolidBody_1_1.pyr5.cnn.B")
    shape                               = five_node_pyramid
}

# +----------------------------------------------------------------------+
# | Volume Set                                                           |
# +----------------------------------------------------------------------+

VOLUME_SET( "CFDAnalysis.SolidBody_1_1.wedge6" ) {
    elements                            = Read("MESH.DIR/CFDAnalysis.SolidBody_1_1.wedge6.cnn.B")
    shape                               = six_node_wedge
}

# +----------------------------------------------------------------------+
# | Surface Set                                                          |
# +----------------------------------------------------------------------+

SURFACE_SET( "CFDAnalysis.SurfaceSet_677566176.tria3.SolidBody_1_1.tet4" ) {
    surfaces                            = Read("MESH.DIR/CFDAnalysis.SurfaceSet_677566176.tria3.SolidBody_1_1.tet4.ebc.B")
    volume_set                          = "CFDAnalysis.SolidBody_1_1.tet4"
    shape                               = three_node_triangle
}

# +----------------------------------------------------------------------+
# | Surface Set                                                          |
# +----------------------------------------------------------------------+

SURFACE_SET( "CFDAnalysis.SurfaceSet_2838655795.tria3.SolidBody_1_1.tet4" ) {
    surfaces                            = Read("MESH.DIR/CFDAnalysis.SurfaceSet_2838655795.tria3.SolidBody_1_1.tet4.ebc.B")
    volume_set                          = "CFDAnalysis.SolidBody_1_1.tet4"
    shape                               = three_node_triangle
}

# +----------------------------------------------------------------------+
# | Surface Set                                                          |
# +----------------------------------------------------------------------+

SURFACE_SET( "CFDAnalysis.SurfaceSet_677566176.tria3.SolidBody_1_1.pyr5" ) {
    surfaces                            = Read("MESH.DIR/CFDAnalysis.SurfaceSet_677566176.tria3.SolidBody_1_1.pyr5.ebc.B")
    volume_set                          = "CFDAnalysis.SolidBody_1_1.pyr5"
    shape                               = three_node_triangle
}

# +----------------------------------------------------------------------+
# | Surface Set                                                          |
# +----------------------------------------------------------------------+

SURFACE_SET( "CFDAnalysis.SurfaceSet_1262928306.tria3.SolidBody_1_1.wedge6" ) {
    surfaces                            = Read("MESH.DIR/CFDAnalysis.SurfaceSet_1262928306.tria3.SolidBody_1_1.wedge6.ebc.B")
    volume_set                          = "CFDAnalysis.SolidBody_1_1.wedge6"
    shape                               = three_node_triangle
}

# +----------------------------------------------------------------------+
# | Surface Set                                                          |
# +----------------------------------------------------------------------+

SURFACE_SET( "CFDAnalysis.SurfaceSet_677566176.quad4.SolidBody_1_1.pyr5" ) {
    surfaces                            = Read("MESH.DIR/CFDAnalysis.SurfaceSet_677566176.quad4.SolidBody_1_1.pyr5.ebc.B")
    volume_set                          = "CFDAnalysis.SolidBody_1_1.pyr5"
    shape                               = four_node_quad
}

# +----------------------------------------------------------------------+
# | Surface Set                                                          |
# +----------------------------------------------------------------------+

SURFACE_SET( "CFDAnalysis.SurfaceSet_677566176.quad4.SolidBody_1_1.wedge6" ) {
    surfaces                            = Read("MESH.DIR/CFDAnalysis.SurfaceSet_677566176.quad4.SolidBody_1_1.wedge6.ebc.B")
    volume_set                          = "CFDAnalysis.SolidBody_1_1.wedge6"
    shape                               = four_node_quad
}

# +----------------------------------------------------------------------+
# | Surface Set                                                          |
# +----------------------------------------------------------------------+

SURFACE_SET( "CFDAnalysis.SurfaceSet_2838655795.quad4.SolidBody_1_1.wedge6" ) {
    surfaces                            = Read("MESH.DIR/CFDAnalysis.SurfaceSet_2838655795.quad4.SolidBody_1_1.wedge6.ebc.B")
    volume_set                          = "CFDAnalysis.SolidBody_1_1.wedge6"
    shape                               = four_node_quad
}
