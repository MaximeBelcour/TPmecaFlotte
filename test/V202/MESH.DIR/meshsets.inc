
# +----------------------------------------------------------------------+
# | Coordinate                                                           |
# +----------------------------------------------------------------------+

COORDINATE {
    coordinates                         = Read("MESH.DIR/V202.crd.B")
}

# +----------------------------------------------------------------------+
# | Volume Set                                                           |
# +----------------------------------------------------------------------+

VOLUME_SET( "V202.SolidBody_1_1.tet4" ) {
    elements                            = Read("MESH.DIR/V202.SolidBody_1_1.tet4.cnn.B")
    shape                               = four_node_tet
}

# +----------------------------------------------------------------------+
# | Volume Set                                                           |
# +----------------------------------------------------------------------+

VOLUME_SET( "V202.SolidBody_1_1.pyr5" ) {
    elements                            = Read("MESH.DIR/V202.SolidBody_1_1.pyr5.cnn.B")
    shape                               = five_node_pyramid
}

# +----------------------------------------------------------------------+
# | Volume Set                                                           |
# +----------------------------------------------------------------------+

VOLUME_SET( "V202.SolidBody_1_1.wedge6" ) {
    elements                            = Read("MESH.DIR/V202.SolidBody_1_1.wedge6.cnn.B")
    shape                               = six_node_wedge
}

# +----------------------------------------------------------------------+
# | Volume Set                                                           |
# +----------------------------------------------------------------------+

VOLUME_SET( "V202.SolidBody_1_1.hex8" ) {
    elements                            = Read("MESH.DIR/V202.SolidBody_1_1.hex8.cnn.B")
    shape                               = eight_node_brick
}

# +----------------------------------------------------------------------+
# | Surface Set                                                          |
# +----------------------------------------------------------------------+

SURFACE_SET( "V202.SurfaceSet_2838655795.tria3.SolidBody_1_1.tet4" ) {
    surfaces                            = Read("MESH.DIR/V202.SurfaceSet_2838655795.tria3.SolidBody_1_1.tet4.ebc.B")
    volume_set                          = "V202.SolidBody_1_1.tet4"
    shape                               = three_node_triangle
}

# +----------------------------------------------------------------------+
# | Surface Set                                                          |
# +----------------------------------------------------------------------+

SURFACE_SET( "V202.SurfaceSet_677566176.tria3.SolidBody_1_1.tet4" ) {
    surfaces                            = Read("MESH.DIR/V202.SurfaceSet_677566176.tria3.SolidBody_1_1.tet4.ebc.B")
    volume_set                          = "V202.SolidBody_1_1.tet4"
    shape                               = three_node_triangle
}

# +----------------------------------------------------------------------+
# | Surface Set                                                          |
# +----------------------------------------------------------------------+

SURFACE_SET( "V202.SurfaceSet_4252425245.tria3.SolidBody_1_1.wedge6" ) {
    surfaces                            = Read("MESH.DIR/V202.SurfaceSet_4252425245.tria3.SolidBody_1_1.wedge6.ebc.B")
    volume_set                          = "V202.SolidBody_1_1.wedge6"
    shape                               = three_node_triangle
}

# +----------------------------------------------------------------------+
# | Surface Set                                                          |
# +----------------------------------------------------------------------+

SURFACE_SET( "V202.SurfaceSet_2114425791.tria3.SolidBody_1_1.wedge6" ) {
    surfaces                            = Read("MESH.DIR/V202.SurfaceSet_2114425791.tria3.SolidBody_1_1.wedge6.ebc.B")
    volume_set                          = "V202.SolidBody_1_1.wedge6"
    shape                               = three_node_triangle
}

# +----------------------------------------------------------------------+
# | Surface Set                                                          |
# +----------------------------------------------------------------------+

SURFACE_SET( "V202.SurfaceSet_2838655795.quad4.SolidBody_1_1.wedge6" ) {
    surfaces                            = Read("MESH.DIR/V202.SurfaceSet_2838655795.quad4.SolidBody_1_1.wedge6.ebc.B")
    volume_set                          = "V202.SolidBody_1_1.wedge6"
    shape                               = four_node_quad
}

# +----------------------------------------------------------------------+
# | Surface Set                                                          |
# +----------------------------------------------------------------------+

SURFACE_SET( "V202.SurfaceSet_677566176.quad4.SolidBody_1_1.wedge6" ) {
    surfaces                            = Read("MESH.DIR/V202.SurfaceSet_677566176.quad4.SolidBody_1_1.wedge6.ebc.B")
    volume_set                          = "V202.SolidBody_1_1.wedge6"
    shape                               = four_node_quad
}

# +----------------------------------------------------------------------+
# | Surface Set                                                          |
# +----------------------------------------------------------------------+

SURFACE_SET( "V202.SurfaceSet_2114425791.quad4.SolidBody_1_1.hex8" ) {
    surfaces                            = Read("MESH.DIR/V202.SurfaceSet_2114425791.quad4.SolidBody_1_1.hex8.ebc.B")
    volume_set                          = "V202.SolidBody_1_1.hex8"
    shape                               = four_node_quad
}
