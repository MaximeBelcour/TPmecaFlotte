<?xml version="1.0" encoding="utf-8"?>
<plotUtilityTemplate name="_plot_autosave_Run2000.1">
    <plot name="Solution Ratio">
        <plotInfo name="xAxis">Time Step</plotInfo>
        <plotInfo name="yAxis">
            <curve color="#e60000" name="eddy-viscosity_eddy-viscosity">Root/Solution Ratio/Final/eddy-viscosity</curve>
            <curve color="#00e6e6" name="pressure_pressure">Root/Solution Ratio/Final/pressure</curve>
            <curve color="#0000e6" name="velocity_velocity">Root/Solution Ratio/Final/velocity</curve>
        </plotInfo>
        <plotInfo name="twinAxis"/>
    </plot>
    <plot name="Residual Ratio">
        <plotInfo name="xAxis">Time Step</plotInfo>
        <plotInfo name="yAxis">
            <curve color="#e60000" name="eddy-viscosity_eddy-viscosity">Root/Residual Ratio/Final/eddy-viscosity</curve>
            <curve color="#00e6e6" name="pressure_pressure">Root/Residual Ratio/Final/pressure</curve>
            <curve color="#0000e6" name="velocity_velocity">Root/Residual Ratio/Final/velocity</curve>
        </plotInfo>
        <plotInfo name="twinAxis"/>
    </plot>
</plotUtilityTemplate>
