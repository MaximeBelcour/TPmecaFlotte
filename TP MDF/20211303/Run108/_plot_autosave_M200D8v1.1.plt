<?xml version="1.0" encoding="utf-8"?>
<plotUtilityTemplate name="_plot_autosave_M200D8v1.1">
    <plot name="Solution Ratio">
        <plotInfo name="xAxis">Time Step</plotInfo>
        <plotInfo name="yAxis">
            <curve color="#e60000" name="eddy-viscosity_eddy-viscosity">Root/Solution Ratio/Final/eddy-viscosity</curve>
            <curve color="#00e6e6" name="pressure_pressure">Root/Solution Ratio/Final/pressure</curve>
            <curve color="#0000e6" name="velocity_velocity">Root/Solution Ratio/Final/velocity</curve>
        </plotInfo>
        <plotInfo name="twinAxis"/>
    </plot>
    <plot name="Residual Ratio">
        <plotInfo name="xAxis">Time Step</plotInfo>
        <plotInfo name="yAxis">
            <curve color="#e60000" name="eddy-viscosity_eddy-viscosity">Root/Residual Ratio/Final/eddy-viscosity</curve>
            <curve color="#00e6e6" name="pressure_pressure">Root/Residual Ratio/Final/pressure</curve>
            <curve color="#0000e6" name="velocity_velocity">Root/Residual Ratio/Final/velocity</curve>
        </plotInfo>
        <plotInfo name="twinAxis"/>
    </plot>
    <plot name="Plot 1">
        <plotInfo name="xAxis">Time Step</plotInfo>
        <plotInfo name="yAxis">
            <curve color="#7f709f" name="entree_pressure">Root/Surface Output/pressure/entree</curve>
            <curve color="#90d468" name="transition_pressure">Root/Surface Output/pressure/transition</curve>
            <curve color="#f2e2f7" name="sortie_pressure">Root/Surface Output/pressure/sortie</curve>
        </plotInfo>
        <plotInfo name="twinAxis"/>
    </plot>
</plotUtilityTemplate>
