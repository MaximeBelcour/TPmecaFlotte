
# +----------------------------------------------------------------------+
# | Coordinate                                                           |
# +----------------------------------------------------------------------+

COORDINATE {
    coordinates                         = Read("MESH.DIR/M10000D8v1.crd.B")
}

# +----------------------------------------------------------------------+
# | Volume Set                                                           |
# +----------------------------------------------------------------------+

VOLUME_SET( "M10000D8v1.SolidBody_1_1.tet4" ) {
    elements                            = Read("MESH.DIR/M10000D8v1.SolidBody_1_1.tet4.cnn.B")
    shape                               = four_node_tet
}

# +----------------------------------------------------------------------+
# | Volume Set                                                           |
# +----------------------------------------------------------------------+

VOLUME_SET( "M10000D8v1.SolidBody_1_1.wedge6" ) {
    elements                            = Read("MESH.DIR/M10000D8v1.SolidBody_1_1.wedge6.cnn.B")
    shape                               = six_node_wedge
}

# +----------------------------------------------------------------------+
# | Surface Set                                                          |
# +----------------------------------------------------------------------+

SURFACE_SET( "M10000D8v1.SurfaceSet_2838655795.tria3.SolidBody_1_1.tet4" ) {
    surfaces                            = Read("MESH.DIR/M10000D8v1.SurfaceSet_2838655795.tria3.SolidBody_1_1.tet4.ebc.B")
    volume_set                          = "M10000D8v1.SolidBody_1_1.tet4"
    shape                               = three_node_triangle
}

# +----------------------------------------------------------------------+
# | Surface Set                                                          |
# +----------------------------------------------------------------------+

SURFACE_SET( "M10000D8v1.SurfaceSet_677566176.tria3.SolidBody_1_1.tet4" ) {
    surfaces                            = Read("MESH.DIR/M10000D8v1.SurfaceSet_677566176.tria3.SolidBody_1_1.tet4.ebc.B")
    volume_set                          = "M10000D8v1.SolidBody_1_1.tet4"
    shape                               = three_node_triangle
}

# +----------------------------------------------------------------------+
# | Surface Set                                                          |
# +----------------------------------------------------------------------+

SURFACE_SET( "M10000D8v1.SurfaceSet_4252425245.tria3.SolidBody_1_1.wedge6" ) {
    surfaces                            = Read("MESH.DIR/M10000D8v1.SurfaceSet_4252425245.tria3.SolidBody_1_1.wedge6.ebc.B")
    volume_set                          = "M10000D8v1.SolidBody_1_1.wedge6"
    shape                               = three_node_triangle
}

# +----------------------------------------------------------------------+
# | Surface Set                                                          |
# +----------------------------------------------------------------------+

SURFACE_SET( "M10000D8v1.SurfaceSet_2114425791.tria3.SolidBody_1_1.wedge6" ) {
    surfaces                            = Read("MESH.DIR/M10000D8v1.SurfaceSet_2114425791.tria3.SolidBody_1_1.wedge6.ebc.B")
    volume_set                          = "M10000D8v1.SolidBody_1_1.wedge6"
    shape                               = three_node_triangle
}

# +----------------------------------------------------------------------+
# | Surface Set                                                          |
# +----------------------------------------------------------------------+

SURFACE_SET( "M10000D8v1.SurfaceSet_2838655795.quad4.SolidBody_1_1.wedge6" ) {
    surfaces                            = Read("MESH.DIR/M10000D8v1.SurfaceSet_2838655795.quad4.SolidBody_1_1.wedge6.ebc.B")
    volume_set                          = "M10000D8v1.SolidBody_1_1.wedge6"
    shape                               = four_node_quad
}

# +----------------------------------------------------------------------+
# | Surface Set                                                          |
# +----------------------------------------------------------------------+

SURFACE_SET( "M10000D8v1.SurfaceSet_677566176.quad4.SolidBody_1_1.wedge6" ) {
    surfaces                            = Read("MESH.DIR/M10000D8v1.SurfaceSet_677566176.quad4.SolidBody_1_1.wedge6.ebc.B")
    volume_set                          = "M10000D8v1.SolidBody_1_1.wedge6"
    shape                               = four_node_quad
}
