<?xml version="1.0" encoding="utf-8"?>
<plotUtilityTemplate name="_plot_autosave_MAT200D02v1.1">
    <plot name="Solution Ratio">
        <plotInfo name="xAxis">Time Step</plotInfo>
        <plotInfo name="yAxis">
            <curve color="#e60000" name="eddy-viscosity_eddy-viscosity">Root/Solution Ratio/Final/eddy-viscosity</curve>
            <curve color="#00e6e6" name="pressure_pressure">Root/Solution Ratio/Final/pressure</curve>
            <curve color="#0000e6" name="velocity_velocity">Root/Solution Ratio/Final/velocity</curve>
        </plotInfo>
        <plotInfo name="twinAxis"/>
    </plot>
    <plot name="Residual Ratio">
        <plotInfo name="xAxis">Time Step</plotInfo>
        <plotInfo name="yAxis">
            <curve color="#e60000" name="eddy-viscosity_eddy-viscosity">Root/Residual Ratio/Final/eddy-viscosity</curve>
            <curve color="#00e6e6" name="pressure_pressure">Root/Residual Ratio/Final/pressure</curve>
            <curve color="#0000e6" name="velocity_velocity">Root/Residual Ratio/Final/velocity</curve>
        </plotInfo>
        <plotInfo name="twinAxis"/>
    </plot>
    <plot name="vitesse">
        <plotInfo name="xAxis">Time Step</plotInfo>
        <plotInfo name="yAxis">
            <curve color="#ff0000" name="Sortie_z_velocity">Root/Surface Output/velocity/z_velocity/Sortie</curve>
            <curve color="#00ff00" name="Transition_z_velocity">Root/Surface Output/velocity/z_velocity/Transition</curve>
            <curve color="#0000ff" name="Entree_z_velocity">Root/Surface Output/velocity/z_velocity/Entree</curve>
        </plotInfo>
        <plotInfo name="twinAxis"/>
    </plot>
    <plot name="pression">
        <plotInfo name="xAxis">Time Step</plotInfo>
        <plotInfo name="yAxis">
            <curve color="#fc3f23" name="Sortie_pressure">Root/Surface Output/pressure/Sortie</curve>
            <curve color="#00ff00" name="Transition_pressure">Root/Surface Output/pressure/Transition</curve>
            <curve color="#0000ff" name="Entree_pressure">Root/Surface Output/pressure/Entree</curve>
        </plotInfo>
        <plotInfo name="twinAxis"/>
    </plot>
</plotUtilityTemplate>
