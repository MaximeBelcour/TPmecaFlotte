# DATE: Sun Mar 14 14:19:37 2021
# VERSION: 2018
#1# INCLUDE "MAT300D02v1.inp"
INCLUDE {
    file = "MESH.DIR/meshsets.inc" ;
}
#2# INCLUDE "MESH.DIR/meshsets.inc"
COORDINATE {
    coordinates = Read( "MESH.DIR/MAT300D02v1.crd.B" ) ;
}
VOLUME_SET( "MAT300D02v1.SolidBody_1_1.tet4" ) {
    shape = four_node_tet ;
    elements = Read( "MESH.DIR/MAT300D02v1.SolidBody_1_1.tet4.cnn.B" ) ;
}
VOLUME_SET( "MAT300D02v1.SolidBody_1_1.wedge6" ) {
    shape = six_node_wedge ;
    elements = Read( "MESH.DIR/MAT300D02v1.SolidBody_1_1.wedge6.cnn.B" ) ;
}
SURFACE_SET( "MAT300D02v1.SurfaceSet_677566176.tria3.SolidBody_1_1.tet4" ) {
    shape = three_node_triangle ;
    volume_set = "MAT300D02v1.SolidBody_1_1.tet4" ;
    surfaces = Read( "MESH.DIR/MAT300D02v1.SurfaceSet_677566176.tria3.SolidBody_1_1.tet4.ebc.B" ) ;
}
SURFACE_SET( "MAT300D02v1.SurfaceSet_2838655795.tria3.SolidBody_1_1.tet4" ) {
    shape = three_node_triangle ;
    volume_set = "MAT300D02v1.SolidBody_1_1.tet4" ;
    surfaces = Read( "MESH.DIR/MAT300D02v1.SurfaceSet_2838655795.tria3.SolidBody_1_1.tet4.ebc.B" ) ;
}
SURFACE_SET( "MAT300D02v1.SurfaceSet_4252425245.tria3.SolidBody_1_1.wedge6" ) {
    shape = three_node_triangle ;
    volume_set = "MAT300D02v1.SolidBody_1_1.wedge6" ;
    surfaces = Read( "MESH.DIR/MAT300D02v1.SurfaceSet_4252425245.tria3.SolidBody_1_1.wedge6.ebc.B" ) ;
}
SURFACE_SET( "MAT300D02v1.SurfaceSet_2114425791.tria3.SolidBody_1_1.wedge6" ) {
    shape = three_node_triangle ;
    volume_set = "MAT300D02v1.SolidBody_1_1.wedge6" ;
    surfaces = Read( "MESH.DIR/MAT300D02v1.SurfaceSet_2114425791.tria3.SolidBody_1_1.wedge6.ebc.B" ) ;
}
SURFACE_SET( "MAT300D02v1.SurfaceSet_677566176.quad4.SolidBody_1_1.wedge6" ) {
    shape = four_node_quad ;
    volume_set = "MAT300D02v1.SolidBody_1_1.wedge6" ;
    surfaces = Read( "MESH.DIR/MAT300D02v1.SurfaceSet_677566176.quad4.SolidBody_1_1.wedge6.ebc.B" ) ;
}
SURFACE_SET( "MAT300D02v1.SurfaceSet_2838655795.quad4.SolidBody_1_1.wedge6" ) {
    shape = four_node_quad ;
    volume_set = "MAT300D02v1.SolidBody_1_1.wedge6" ;
    surfaces = Read( "MESH.DIR/MAT300D02v1.SurfaceSet_2838655795.quad4.SolidBody_1_1.wedge6.ebc.B" ) ;
}
ANALYSIS {
    title = "MAT300D02v1" ;
    type = static ;
}
#2# END INCLUDE "MESH.DIR/meshsets.inc"
EQUATION {
    flow = navier_stokes ;
    absolute_pressure_offset = 0 ;
    turbulence = spalart_allmaras ;
    mesh = eulerian ;
}
AUTO_SOLUTION_STRATEGY {
    max_time_steps = 100 ;
    initial_time_increment = 10000000000 ;
    convergence_tolerance = 0.001 ;
    num_krylov_vectors = 10 ;
    flow = on ;
    turbulence = on ;
    relaxation_factor = 0.4 ;
}
#2# INCLUDE "MAT300D02v1.ss.inc"
TIME_SEQUENCE {
    final_time = 0 ;
    min_time_steps = 1 ;
    max_time_steps = 100 ;
    min_cases = 1 ;
    max_cases = 1 ;
    convergence_tolerance = 0.001 ;
    optimization_convergence_tolerance = 0.0001 ;
    termination_delay = 0 ;
    lhs_update_initial_times = 1 ;
    lhs_update_frequency = 1 ;
    min_stagger_iterations = 1 ;
    max_stagger_iterations = 1 ;
    stagger_convergence_tolerance = 1 ;
    stagger_lhs_update_frequency = 1 ;
    allow_stagger_reordering = off ;
    staggers = { "flow",
                 "turbulence" } ;
}
STAGGER( "flow" ) {
    equation = flow ;
    min_stagger_iterations = 1 ;
    max_stagger_iterations = 1 ;
    convergence_tolerance = 0.1 ;
    lhs_update_frequency = 1 ;
    linear_solver = gmres ;
    min_linear_solver_iterations = 10 ;
    max_linear_solver_iterations = 1000 ;
    num_krylov_vectors = 10 ;
    linear_solver_tolerance = 0.1 ;
    pressure_projection_tolerance = 0.01 ;
    velocity_projection_tolerance = 0.1 ;
    projection = off ;
    pressure_projection = on ;
    velocity_projection = off ;
    pressure_algebraic_multigrid = off ;
}
STAGGER( "turbulence" ) {
    equation = turbulence ;
    min_stagger_iterations = 1 ;
    max_stagger_iterations = 1 ;
    convergence_tolerance = 0.1 ;
    lhs_update_frequency = 1 ;
    linear_solver = gmres ;
    min_linear_solver_iterations = 10 ;
    max_linear_solver_iterations = 1000 ;
    num_krylov_vectors = 40 ;
    linear_solver_tolerance = 0.01 ;
    projection = on ;
}
TIME_INCREMENT {
    initial_time_increment = 10000000000 ;
    auto_time_increment = off ;
    local_time_increment = off ;
    min_time_increment = 0 ;
    max_time_increment = 0 ;
    cfl_control = off ;
    cfl_number = 1000 ;
    min_cfl_number = 0 ;
    initial_cfl_number = 1 ;
    time_increment_decrease_factor = 0.25 ;
    time_increment_increase_factor = 1.25 ;
    time_increment_increase_delay = 4 ;
    min_time_increment_ratio = 0.1 ;
    multiplier_function = "none" ;
}
TIME_INTEGRATION {
    predictor = same_v ;
    time_integration_order = first ;
    high_frequency_damping_factor = 1 ;
    pressure_damping_type = max ;
    lumped_mass_factor = 1 ;
    initialize_acceleration = off ;
}
LINEAR_SOLVER_PARAMETERS {
    lhs_storage = reduced_memory_sparse ;
    min_num_iteration_ratio = 0.5 ;
    pressure_precedence_factor = 1 ;
    num_pressure_projection_vectors = 11 ;
    num_velocity_projection_vectors = 5 ;
    num_flow_projection_vectors = 5 ;
    num_viscoelastic_stress_projection_vectors = 5 ;
    num_temperature_projection_vectors = 11 ;
    num_radiation_projection_vectors = 10 ;
    num_temperature_flow_projection_vectors = 4 ;
    num_species_projection_vectors = 10 ;
    num_field_projection_vectors = 10 ;
    num_turbulence_projection_vectors = 10 ;
    num_transition_projection_vectors = 10 ;
    num_mesh_displacement_projection_vectors = 10 ;
    pressure_lhs_inverse_order = 5 ;
    velocity_lhs_inverse_order = 5 ;
    flow_lhs_inverse_order = 5 ;
    temperature_lhs_inverse_order = 6 ;
    pressure_regularization_factor = 1 ;
    velocity_regularization_factor = 0.5 ;
    flow_regularization_factor = 3.669296667619248e-005 ;
    viscoelastic_stress_regularization_factor = 3.669296667619248e-006 ;
    temperature_regularization_factor = 3.669296667619248e-006 ;
    temperature_flow_regularization_factor = 3.669296667619248e-005 ;
    species_regularization_factor = 0 ;
    field_regularization_factor = 0 ;
    turbulence_regularization_factor = 0 ;
    transition_regularization_factor = 0 ;
    mesh_displacement_regularization_factor = 0 ;
    pressure_update_factor = 1 ;
    velocity_update_factor = 0.605 ;
    viscoelastic_stress_update_factor = 1 ;
    temperature_update_factor = 0.8999999999999999 ;
    species_update_factor = 1 ;
    field_update_factor = 1 ;
    turbulence_update_factor = 1 ;
    transition_update_factor = 1 ;
    mesh_displacement_update_factor = 1 ;
    radiation_update_factor = 1 ;
    max_pressure_update = 0 ;
    max_velocity_update = 0 ;
    max_viscoelastic_stress_update = 0 ;
    max_temperature_update = 0 ;
    max_species_update = 0 ;
    max_field_update = 0 ;
    max_turbulence_update = 0 ;
    max_transition_update = 0 ;
    max_mesh_displacement_update = 0 ;
    max_radiation_update = 0 ;
    max_reverse_update_factor = 0 ;
}
CONVERGENCE_CHECK_PARAMETERS {
    pressure_residual_check = standard ;
    pressure_solution_increment_check = looser_by_10 ;
    velocity_residual_check = standard ;
    velocity_solution_increment_check = looser_by_10 ;
    viscoelastic_stress_residual_check = looser_by_10 ;
    viscoelastic_stress_solution_increment_check = looser_by_10 ;
    temperature_residual_check = standard ;
    temperature_solution_increment_check = looser_by_10 ;
    radiation_residual_check = none ;
    radiation_solution_increment_check = looser_by_10 ;
    species_residual_check = standard ;
    species_solution_increment_check = looser_by_10 ;
    field_residual_check = standard ;
    field_solution_increment_check = looser_by_10 ;
    phasefield_residual_check = standard ;
    phasefield_solution_increment_check = looser_by_10 ;
    levelset_residual_check = looser_by_10 ;
    levelset_solution_increment_check = looser_by_100 ;
    redistancing_residual_check = looser_by_100 ;
    redistancing_solution_increment_check = looser_by_100 ;
    turbulence_residual_check = looser_by_10 ;
    turbulence_solution_increment_check = looser_by_100 ;
    transition_residual_check = looser_by_100 ;
    transition_solution_increment_check = looser_by_1000 ;
    mesh_displacement_residual_check = looser_by_10 ;
    mesh_displacement_solution_increment_check = looser_by_100 ;
}
ALGEBRAIC_MULTIGRID_PARAMETERS {
    pressure_standard_interpolation = on ;
    pressure_truncated_interpolation = on ;
    pressure_negative_coupling_tolerance = 0.6 ;
    pressure_positive_coupling_tolerance = 1 ;
    pressure_truncation_tolerance = 0.1 ;
    max_pressure_final_matrix = 100 ;
    pressure_eigenvalue_tolerance = 0.01 ;
    max_pressure_eigenvalue_iterations = 20 ;
    pressure_smoothing_order = 2 ;
    pressure_chebyshev_max_min_ratio = 10 ;
    pressure_jacobi_relaxation_factor = 0.25 ;
    pressure_smoothing_type = chebyshev ;
    pressure_positive_negative_separate = off ;
    pressure_givens_scaling = on ;
    pressure_setup_tolerance = 0 ;
    velocity_standard_interpolation = on ;
    velocity_truncated_interpolation = on ;
    velocity_negative_coupling_tolerance = 0.5 ;
    velocity_positive_coupling_tolerance = 1 ;
    velocity_truncation_tolerance = 0.1 ;
    max_velocity_final_matrix = 100 ;
    velocity_num_krylov_vectors = 30 ;
    velocity_smoothing_order = 2 ;
    velocity_chebyshev_max_min_ratio = 10 ;
    velocity_jacobi_relaxation_factor = 0.25 ;
    velocity_smoothing_type = chebyshev ;
    velocity_positive_negative_separate = on ;
    velocity_givens_scaling = off ;
    velocity_setup_tolerance = 0 ;
    flow_standard_interpolation = off ;
    flow_truncated_interpolation = on ;
    flow_negative_coupling_tolerance = 0.5 ;
    flow_positive_coupling_tolerance = 1 ;
    flow_truncation_tolerance = 0.1 ;
    max_flow_final_matrix = 100 ;
    flow_num_krylov_vectors = 30 ;
    flow_smoothing_order = 2 ;
    flow_chebyshev_max_min_ratio = 10 ;
    flow_jacobi_relaxation_factor = 0.2 ;
    flow_smoothing_type = jacobi ;
    flow_positive_negative_separate = on ;
    flow_givens_scaling = off ;
    flow_setup_tolerance = 0 ;
    temperature_standard_interpolation = on ;
    temperature_truncated_interpolation = on ;
    temperature_negative_coupling_tolerance = 0.5 ;
    temperature_positive_coupling_tolerance = 1 ;
    temperature_truncation_tolerance = 0.1 ;
    max_temperature_final_matrix = 100 ;
    temperature_num_krylov_vectors = 30 ;
    temperature_smoothing_order = 2 ;
    temperature_chebyshev_max_min_ratio = 10 ;
    temperature_jacobi_relaxation_factor = 0.25 ;
    temperature_smoothing_type = chebyshev ;
    temperature_positive_negative_separate = on ;
    temperature_givens_scaling = off ;
    temperature_setup_tolerance = 0 ;
    species_standard_interpolation = on ;
    species_truncated_interpolation = on ;
    species_negative_coupling_tolerance = 0.5 ;
    species_positive_coupling_tolerance = 1 ;
    species_truncation_tolerance = 0.1 ;
    max_species_final_matrix = 100 ;
    species_num_krylov_vectors = 30 ;
    species_smoothing_order = 2 ;
    species_chebyshev_max_min_ratio = 10 ;
    species_jacobi_relaxation_factor = 0.25 ;
    species_smoothing_type = chebyshev ;
    species_positive_negative_separate = on ;
    species_givens_scaling = off ;
    species_setup_tolerance = 0 ;
    turbulence_standard_interpolation = on ;
    turbulence_truncated_interpolation = on ;
    turbulence_negative_coupling_tolerance = 0.5 ;
    turbulence_positive_coupling_tolerance = 1 ;
    turbulence_truncation_tolerance = 0.1 ;
    max_turbulence_final_matrix = 100 ;
    turbulence_num_krylov_vectors = 30 ;
    turbulence_smoothing_order = 3 ;
    turbulence_chebyshev_max_min_ratio = 10 ;
    turbulence_jacobi_relaxation_factor = 0.25 ;
    turbulence_smoothing_type = chebyshev ;
    turbulence_positive_negative_separate = on ;
    turbulence_givens_scaling = off ;
    turbulence_setup_tolerance = 0 ;
    transition_standard_interpolation = on ;
    transition_truncated_interpolation = on ;
    transition_negative_coupling_tolerance = 0.5 ;
    transition_positive_coupling_tolerance = 1 ;
    transition_truncation_tolerance = 0.1 ;
    max_transition_final_matrix = 100 ;
    transition_num_krylov_vectors = 30 ;
    transition_smoothing_order = 3 ;
    transition_chebyshev_max_min_ratio = 10 ;
    transition_jacobi_relaxation_factor = 0.25 ;
    transition_smoothing_type = chebyshev ;
    transition_positive_negative_separate = on ;
    transition_givens_scaling = off ;
    transition_setup_tolerance = 0 ;
    mesh_standard_interpolation = on ;
    mesh_truncated_interpolation = on ;
    mesh_negative_coupling_tolerance = 0.5 ;
    mesh_positive_coupling_tolerance = 1 ;
    mesh_truncation_tolerance = 0.1 ;
    max_mesh_final_matrix = 100 ;
    mesh_eigenvalue_tolerance = 0.01 ;
    max_mesh_eigenvalue_iterations = 20 ;
    mesh_smoothing_order = 2 ;
    mesh_chebyshev_max_min_ratio = 10 ;
    mesh_jacobi_relaxation_factor = 0.25 ;
    mesh_smoothing_type = chebyshev ;
    mesh_positive_negative_separate = on ;
    mesh_givens_scaling = off ;
    mesh_setup_tolerance = 0 ;
    viscoelastic_standard_interpolation = on ;
    viscoelastic_truncated_interpolation = on ;
    viscoelastic_negative_coupling_tolerance = 0.5 ;
    viscoelastic_positive_coupling_tolerance = 0.3 ;
    viscoelastic_truncation_tolerance = 0.1 ;
    max_viscoelastic_final_matrix = 100 ;
    viscoelastic_num_krylov_vectors = 30 ;
    viscoelastic_smoothing_order = 2 ;
    viscoelastic_chebyshev_max_min_ratio = 10 ;
    viscoelastic_jacobi_relaxation_factor = 0.25 ;
    viscoelastic_smoothing_type = jacobi ;
    viscoelastic_positive_negative_separate = on ;
    viscoelastic_givens_scaling = off ;
    viscoelastic_setup_tolerance = 0 ;
}
TIME_INTEGRATION {
    initialize_stokes = on ;
    initialize_turbulence = on ;
}
#2# END INCLUDE "MAT300D02v1.ss.inc"
DENSITY_MODEL( "Mat300" ) {
    type = constant ;
    density = 300 ;
    expansivity_type = constant ;
    expansivity = 1 ;
    reference_temperature = 273.14 ;
    reference_pressure = 0 ;
    specific_heat_ratio = 1.4 ;
    gas_constant = 286.6 ;
    isothermal_compressibility = 0 ;
    curve_fit_variable = x_coordinate ;
}
CONDUCTIVITY_MODEL( "Mat300" ) {
    type = constant ;
    conductivity = 0 ;
    prandtl_number = 0.71 ;
    curve_fit_variable = x_coordinate ;
    anisotropic_curve_fit_variable = x_coordinate ;
    turbulent_prandtl_number = 0.91 ;
}
SPECIFIC_HEAT_MODEL( "Mat300" ) {
    type = constant ;
    specific_heat = 1 ;
    curve_fit_variable = temperature ;
    piecewise_polynomial_variable = temperature ;
    latent_heat_type = none ;
    latent_heat = 0 ;
    latent_heat_temperature = 0 ;
    latent_heat_temperature_interval = 0 ;
}
VISCOSITY_MODEL( "Mat300" ) {
    type = constant ;
    viscosity = 0.01 ;
    power_law_viscosity = 0 ;
    power_law_time_constant = 0 ;
    power_law_index = 1 ;
    power_law_lower_strain_rate = 0 ;
    bingham_viscosity = 1 ;
    bingham_yield_stress = 0 ;
    bingham_stress_growth_exponent = 500 ;
    bingham_time_constant = 1 ;
    bingham_index = 1 ;
    bingham_infinite_shear_viscosity = 0 ;
    carreau_zero_shear_viscosity = 0 ;
    carreau_infinite_shear_viscosity = 0 ;
    carreau_time_constant = 0 ;
    carreau_index = 1 ;
    carreau_transition_index = 2 ;
    curve_fit_variable = x_coordinate ;
}
MATERIAL_MODEL( "Mat300" ) {
    type = fluid ;
    density_model = "Mat300" ;
    specific_heat_model = "Mat300" ;
    viscosity_model = "Mat300" ;
    conductivity_model = "Mat300" ;
}
ELEMENT_SET( "DefaultPart SolidBody_1_1" ) {
    volume_sets = { "MAT300D02v1.SolidBody_1_1.tet4",
                    "MAT300D02v1.SolidBody_1_1.wedge6" } ;
    medium = fluid ;
    quadrature = full ;
    material_model = "Mat300" ;
    num_shell_layers = 1 ;
    shell_thickness_type = constant ;
    viscous_heating = off ;
    compression_heating = off ;
    enhanced_strain = off ;
    num_enhanced_modes = 0 ;
    residual_control = on ;
    oscillation_control = on ;
    mesh_distortion_correction_factor = 0 ;
    mesh_distortion_tolerance = 0 ;
}
NODAL_INITIAL_CONDITION( "pressure" ) {
    selection_type = all ;
    type = constant ;
    default_value = 0 ;
    satisfy_boundary_condition = off ;
    precedence = 0 ;
}
SIMPLE_BOUNDARY_CONDITION( "Default Wall" ) {
    surface_sets = { "MAT300D02v1.SurfaceSet_2114425791.tria3.SolidBody_1_1.wedge6",
                     "MAT300D02v1.SurfaceSet_4252425245.tria3.SolidBody_1_1.wedge6" } ;
    type = auto_wall ;
    precedence = 1 ;
    turbulence_wall_type = wall_function ;
    roughness_height = 0 ;
    wall_function_friction_factor = 1 ;
    mesh_displacement_type = fixed ;
    split_internal_surfaces = on ;
    gap_factor = 1 ;
    gap = 0 ;
    crease_angle = 90 ;
    active_type = all ;
}
SIMPLE_BOUNDARY_CONDITION( "Inlet" ) {
    surface_sets = { "MAT300D02v1.SurfaceSet_677566176.quad4.SolidBody_1_1.wedge6",
                     "MAT300D02v1.SurfaceSet_677566176.tria3.SolidBody_1_1.tet4" } ;
    type = inflow ;
    inflow_type = velocity ;
    precedence = 1 ;
    inflow_velocity_type = cartesian ;
    x_velocity = 0 ;
    y_velocity = 0 ;
    z_velocity = 1 ;
    turbulence_input_type = auto ;
    turbulence_flow_type = internal ;
    turbulence_intensity_type = auto ;
    percent_turbulence_intensity = 0 ;
    turbulence_velocity_scale = 0 ;
    turbulence_length_scale = 0 ;
    turbulence_viscosity_ratio = 1 ;
    eddy_viscosity = 0.036742 ;
    non_reflecting_factor = 0 ;
    mesh_displacement_type = fixed ;
    active_type = all ;
}
SURFACE_OUTPUT( "Inlet - Output" ) {
    surface_sets = { "MAT300D02v1.SurfaceSet_677566176.quad4.SolidBody_1_1.wedge6",
                     "MAT300D02v1.SurfaceSet_677566176.tria3.SolidBody_1_1.tet4" } ;
    integrated_output_frequency = 1 ;
    integrated_output_time_interval = 0 ;
    statistics_output_frequency = 1 ;
    statistics_output_time_interval = 0 ;
    nodal_output_frequency = 0 ;
    nodal_output_time_interval = 0 ;
    num_saved_states = 0 ;
}
SIMPLE_BOUNDARY_CONDITION( "Outlet" ) {
    surface_sets = { "MAT300D02v1.SurfaceSet_2838655795.quad4.SolidBody_1_1.wedge6",
                     "MAT300D02v1.SurfaceSet_2838655795.tria3.SolidBody_1_1.tet4" } ;
    type = outflow ;
    back_flow_conditions = off ;
    precedence = 1 ;
    pressure = 101325 ;
    pressure_loss_factor = 0 ;
    hydrostatic_pressure = off ;
    hydrostatic_pressure_origin = { 0, 0, 0; } ;
    non_reflecting_factor = 0 ;
    mesh_displacement_type = fixed ;
    active_type = all ;
}
SURFACE_OUTPUT( "Outlet - Output" ) {
    surface_sets = { "MAT300D02v1.SurfaceSet_2838655795.quad4.SolidBody_1_1.wedge6",
                     "MAT300D02v1.SurfaceSet_2838655795.tria3.SolidBody_1_1.tet4" } ;
    integrated_output_frequency = 1 ;
    integrated_output_time_interval = 0 ;
    statistics_output_frequency = 1 ;
    statistics_output_time_interval = 0 ;
    nodal_output_frequency = 0 ;
    nodal_output_time_interval = 0 ;
    num_saved_states = 0 ;
}
NODAL_OUTPUT {
    output_frequency = 100 ;
    output_initial_condition = off ;
    continuous_output = off ;
}
SURFACE_OUTPUT( "Sortie" ) {
    surface_sets = { "MAT300D02v1.SurfaceSet_2838655795.quad4.SolidBody_1_1.wedge6",
                     "MAT300D02v1.SurfaceSet_2838655795.tria3.SolidBody_1_1.tet4" } ;
    integrated_output_frequency = 1 ;
    integrated_output_time_interval = 0 ;
    statistics_output_frequency = 1 ;
    statistics_output_time_interval = 0 ;
    nodal_output_frequency = 0 ;
    nodal_output_time_interval = 0 ;
    num_saved_states = 0 ;
}
SURFACE_OUTPUT( "Transition" ) {
    surface_sets = { "MAT300D02v1.SurfaceSet_2114425791.tria3.SolidBody_1_1.wedge6" } ;
    integrated_output_frequency = 1 ;
    integrated_output_time_interval = 0 ;
    statistics_output_frequency = 1 ;
    statistics_output_time_interval = 0 ;
    nodal_output_frequency = 0 ;
    nodal_output_time_interval = 0 ;
    num_saved_states = 0 ;
}
SURFACE_OUTPUT( "Entree" ) {
    surface_sets = { "MAT300D02v1.SurfaceSet_677566176.quad4.SolidBody_1_1.wedge6",
                     "MAT300D02v1.SurfaceSet_677566176.tria3.SolidBody_1_1.tet4" } ;
    integrated_output_frequency = 1 ;
    integrated_output_time_interval = 0 ;
    statistics_output_frequency = 1 ;
    statistics_output_time_interval = 0 ;
    nodal_output_frequency = 0 ;
    nodal_output_time_interval = 0 ;
    num_saved_states = 0 ;
}
RUN
