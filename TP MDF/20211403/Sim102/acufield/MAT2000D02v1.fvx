--------------------------------------------------------------------------------
--  Copyright (c) 2018 Intelligent Light                                      --
--  All rights reserved.                                                      --
--                                                                            --
--  This sample FVX script is not supported by Intelligent Light              --
--  and Intelligent Light provides no warranties or assurances                --
--  about its fitness or merchantability.  It is provided at no               --
--  cost and is for demonstration purposes only.                              --
--------------------------------------------------------------------------------



--------------------------------------------------------------------------------
--            DATA INPUT
--------------------------------------------------------------------------------
local datasets_info_table = {}
datasets_info_table[1] = read_dataset( {
    data_format = "acusolve_direct",
    input_parameters = {
        name = "C:/Users/Maxime Belcour/Documents/GitHub/TPmecaFlotte/TP MDF/20211403/Sim102/MAT2000D02v1.1.Log",
        options = {
            input_mode = "replace",
            grid_processing = "balanced",
            boundary_only = "off",
            duplicate_boundaries = "off",
            extended_variables = "on"
        } -- options
    } -- input_parameters
} ) -- read_dataset

-- print_dataset_table( datasets_info_table[1] )


--------------------------------------------------------------------------------
--            VIEWING PARAMETERS
--------------------------------------------------------------------------------
fv_script("RESTART VIEW C:/Users/Maxime Belcour/Documents/GitHub/TPmecaFlotte/TP MDF/20211403/Sim102/acufield/MAT2000D02v1.vct")

--------------------------------------------------------------------------------
--            COLORMAP
--------------------------------------------------------------------------------
fv_script("RESTART COLOR C:/Users/Maxime Belcour/Documents/GitHub/TPmecaFlotte/TP MDF/20211403/Sim102/acufield/MAT2000D02v1.map")

--------------------------------------------------------------------------------
--            BOUNDARY SURFACES
--------------------------------------------------------------------------------
local boundary_surfs={}

boundary_surfs[1] = create_boundary(
    {
        scalar_colormap = {
            invert = "off",
            filled_contour = "off",
            name = "spectrum",
            log_scale = "off",
        }, -- scalar_colormap
        transparency = 0,
        show_legend = "off",
        number_of_contours = 16,
        scalar_range = {
            abs_max = 101661.4609375,
            local_min = 101122.359375,
            min = 101099.4609375,
            abs_min = 101099.4609375,
            max = 101661.4609375,
            local_max = 101661.4609375,
            use_local = "off",
        }, -- scalar_range
        visibility = "on",
        line_type = "thin",
        geometric_color = "black",
        types = {
            "OSF: Inlet - Output",
            "OSF: Outlet - Output",
            "OSF: Transition",
            "OSF: AUTO, DefaultPart SolidBody_1_1, wall",
        }, -- types
        display_type = "smooth_shading",
        contours = "none",
        vector_func = "none",
        show_mesh = "on",
        scalar_func = "pressure",
        dataset = 1,
        threshold_func = "none",
    }
) -- boundary_surfs[1]


--------------------------------------------------------------------------------
--            PRESENTATION RENDERING
--------------------------------------------------------------------------------
fv_script("RESTART PRESENTATION C:/Users/Maxime Belcour/Documents/GitHub/TPmecaFlotte/TP MDF/20211403/Sim102/acufield/MAT2000D02v1.prd")

--------------------------------------------------------------------------------
--            DATASET QUERIES
--------------------------------------------------------------------------------
local boundary_types_DS={}
local scalar_functions_DS={}
local vector_functions_DS={}
local surface_scalar_functions_DS={}
local surface_vector_functions_DS={}

print ("")
-- BOUNDARY TYPES FOR DATASET 1
boundary_types_DS[1] = get_all_boundary_types( 1 )
if getn( boundary_types_DS[1] ) ~= 0 then
    print ("BOUNDARY TYPES FOR DATASET 1")
    for i, v in boundary_types_DS[1] do
        if type(v) == "string" then
            print ("    "..v)
        end
    end
    print ("")
end

-- SCALAR FUNCTIONS FOR DATASET 1
scalar_functions_DS[1] = get_scalar_functions( 1 )
if getn( scalar_functions_DS[1]) ~= 0 then
    print("SCALAR FUNCTIONS FOR DATASET 1")
    for i, v in scalar_functions_DS[1] do
        if type(v) == "string" then
            print ("    "..v)
        end
    end
    print ("")
end

-- VECTOR FUNCTIONS FOR DATASET 1
vector_functions_DS[1] = get_vector_functions( 1 )
if getn( vector_functions_DS[1]) ~= 0 then
    print("VECTOR FUNCTIONS FOR DATASET 1")
    for i, v in vector_functions_DS[1] do
        if type(v) == "string" then
            print ("    "..v)
        end
    end
    print ("")
end

-- SURFACE SCALAR FUNCTIONS FOR DATASET 1
surface_scalar_functions_DS[1] = get_surface_scalar_functions( 1 )
if getn( surface_scalar_functions_DS[1]) ~= 0 then
    print("SURFACE SCALAR FUNCTIONS FOR DATASET 1")
    for i, v in surface_scalar_functions_DS[1] do
        if type(v) == "string" then
            print ("    "..v)
        end
    end
    print ("")
end

-- SURFACE VECTOR FUNCTIONS FOR DATASET 1
surface_vector_functions_DS[1] = get_surface_vector_functions( 1 )
if getn( surface_vector_functions_DS[1]) ~= 0 then
    print("SURFACE VECTOR FUNCTIONS FOR DATASET 1")
    for i, v in surface_vector_functions_DS[1] do
        if type(v) == "string" then
            print ("    "..v)
        end
    end
    print ("")
end

print("")


--------------------------------------------------------------------------------
--            GLOBAL QUERIES
--------------------------------------------------------------------------------
-- DEFAULT COLORTABLE AND COLORTABLE
local print_colortable = function( tbl_name, tbl )
    if getn(tbl) ~= 0 then
       for i, v in tbl do
           -- Skip 'n', the size of the the table
           if type(i) == "number" and type(v) == "table" then
               print("    "..tbl_name.."["..i.."] = {")
               if v.name then
                   print("        name = "..v.name..",")
               end
               print("        red = "..v.red..",")
               print("        green = "..v.green..",")
               print("        blue = "..v.blue)
               print("    }\n")
           end
       end
       print("")
    end
end -- print_colortable

local colortable = query_colortable()
print("")
print("COLORTABLE")
print_colortable( "colortable", colortable )


local default_colortable = query_default_colortable()
print("DEFAULT COLORTABLE")
print_colortable("default_colortable", default_colortable)

-- STREAMLINE AND PARTICLE PATHS DISPLAY
local streamline_display = query_streamline_display()

print ("streamline_display = {")
for i, v in streamline_display do
    if type(v) == "string" then
       print ("    "..tostring(i).." = \""..tostring(v).."\",")
    elseif type(v) == "number" then
       print ("    "..tostring(i).." = "..tostring(v)..",")
    end
end
print ("} -- streamline_display")

print("")
local particle_paths_display = query_particle_paths_display()
print ("particle_paths_display = {")
for i, v in particle_paths_display do
    if type(v) == "string" then
       print ("    "..tostring(i).." = \""..tostring(v).."\",")
    elseif type(v) == "number" then
       print ("    "..tostring(i).." = "..tostring(v)..",")
    end
end
print ("} -- particle_paths_display")
